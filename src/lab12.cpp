#include <iostream>
#include "Set/BitmaskSet.h"
#include "Set/LinkedListSet.h"


using namespace std;


BitmaskSet<int> inputUniversum() {
    LinkedListSet<int> U;
    cout << "Input values for universum, enter -1 to stop:" << endl;
    while (true) {
        int e;
        cin >> e;
        if (e == -1) {
            cout << "Done" << endl;
            break;
        }
        U.add(e);
    }
    cout << "Universum = " << U << endl;
    auto beg = U.begin(), end = U.end();
    return BitmaskSet<int>(U.size(), beg, end);
}


void inputSubset(Set<int>& x, const Set<int>& U, const string& name) {
    cout << "Input values for " << name << ", enter -1 to stop:" << endl;
    while (true) {
        int e;
        cin >> e;
        if (e == -1) {
            cout << "Done" << endl;
            break;
        }
        if (U.contains(e)) {
            x.add(e);
        } else {
            cout << "Value " << e << " rejected: not in universum" << endl;
        }
    }
    cout << name << " = " << x << endl;
}


int main(int argc, char** argv) {
    BitmaskSet<int> U = inputUniversum();
    BitmaskSet<int> *A, *B, *C;
    A = U.copy();
    A->clear();
    inputSubset(*A, U, "A");

    B = U.copy();
    B->clear();
    inputSubset(*B, U, "B");

    C = U.copy();
    C->clear();
    inputSubset(*C, U, "C");

    BitmaskSet<int> *R = A->copy();
    R->intersect(*B);
    R->symmetric_difference(*C);
    cout << "A = " << *A << endl;
    cout << "B = " << *B << endl;
    cout << "C = " << *C << endl;
    cout << "(A intersection B) symm-diff C = " << *R << endl;
}