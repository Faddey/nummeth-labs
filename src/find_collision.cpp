
#include <iostream>
#include "lab22/key.h"


int main(int argc, char **argv) {
    Key k{12, 43};
    SubstHasher hasher;
    unsigned h = hasher(k);
    unsigned total = 0;
    for (int i = 0; i < 256; ++i) {
        for (int j = 0; j < 256; ++j) {
            Key k2{i, j};
            unsigned h2 = hasher(k2);
            if (h == h2) {
                total++;
                std::cout << "hash(" << k2 << ") -> " << h2 << std::endl;
            }
        }
    }
    std::cout << "totally " << total << " collisions" << std::endl;
}
