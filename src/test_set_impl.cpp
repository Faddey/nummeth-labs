#include <iostream>
#include <string>
#include <vector>
#include "Set/Set.h"
#include "Set/LinkedListSet.h"
#include "Set/BitmaskSet.h"

using namespace std;


void testSet(Set<int>& X);


int main(int argc, char **argv) {
    cout.setf(ios::unitbuf);

    cout << "====================================================" << endl;
    cout << "================== Linked list set =================" << endl;
    cout << "====================================================" << endl;
    auto lls = LinkedListSet<int>();
    testSet(lls);

    cout << endl << endl;

    cout << "====================================================" << endl;
    cout << "==================== Bitmask set ===================" << endl;
    cout << "====================================================" << endl;
    int universum[20] = {};
    for (int i = 0; i < 20; ++i) universum[i] = i+1;
    auto bms = BitmaskSet<int>(universum, universum+20);
    testSet(bms);
    bms.invert();
    cout << "inv X = " << bms << endl;

    return 0;
}


void testSet(Set<int>& X) {
    X.assign({1, 10, 2, 9, 3, 8, 4, 7, 5, 6});

    cout << "X = " << X << endl;
    cout << "|X| = " << X.size() << endl << endl;

    cout << "add(5) -> " << X.add(5) << endl;
    cout << "contains(15) -> " << X.contains(15) << endl;
    cout << "add(15) -> " << X.add(15) << endl;
    cout << "contains(15) -> " << X.contains(15) << endl;
    cout << "X = " << X << endl;
    cout << "|X| = " << X.size() << endl << endl;

    cout << "remove(7) -> " << X.remove(7) << endl;
    cout << "X = " << X << endl;
    cout << "|X| = " << X.size() << endl << endl;

    auto it = X.begin();
    it ++ ++ ++;
    cout << "remove by iterator pointing to " << *it << " -> ";
    X.remove(it);
    cout << "iterator points to " << *it << endl;
    cout << "X = " << X << endl;
    cout << "|X| = " << X.size() << endl << endl;


    Set<int>* Y = X.copy();
    Y->assign({6, 7, 8, 9, 10, 11, 12, 13, 14});
    Set<int>* R = X.copy();

    cout << "------------------ INPLACE OPS ------------------" << endl;
    cout << "X = " << X << endl;
    cout << "Y = " << *Y << endl;
    R->assign(X);
    R->subtract(*Y);
    cout << "subtraction: " << *R << " (" << R->size() << " items)" << endl;
    R->assign(X);
    R->intersect(*Y);
    cout << "intersection: " << *R << " (" << R->size() << " items)" << endl;
    R->assign(X);
    R->symmetric_difference(*Y);
    cout << "symmetric_difference: " << *R << " (" << R->size() << " items)" << endl;
    cout << endl;

    cout << "------------------ REPLACE OPS ------------------" << endl;
    cout << "X = " << X << endl;
    cout << "Y = " << *Y << endl;
    R->subtraction(X, *Y);
    cout << "subtraction: " << *R << " (" << R->size() << " items)" << endl;
    R->intersection(X, *Y);
    cout << "intersection: " << *R << " (" << R->size() << " items)" << endl;
    R->symmetric_difference(X, *Y);
    cout << "symmetric_difference: " << *R << " (" << R->size() << " items)" << endl;
    cout << endl;

    delete Y;
    delete R;
}
