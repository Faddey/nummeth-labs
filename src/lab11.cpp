#include <iostream>
#include "Set/LinkedListSet.h"


using namespace std;


int main(int argc, char** argv) {
    LinkedListSet<int> U, A, B;
    for (int i = 0; i < 20; ++i) {
        U.add(i);
    }
    for (int i = 0; i < 10; ++i) {
        A.add(i);
    }
    for (int i = 5; i < 15; ++i) {
        B.add(i);
    }

    LinkedListSet<int> *R = new LinkedListSet<int>();
    R->subtraction(U, B);
    R->symmetric_difference(A);
    cout << "A = " << A << endl;
    cout << "B = " << B << endl;
    cout << "A symm-diff (inv B) = " << *R << endl;
}