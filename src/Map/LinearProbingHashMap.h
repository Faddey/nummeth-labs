#ifndef NUMMETH_LINEARPROBINGHASHMAP_H
#define NUMMETH_LINEARPROBINGHASHMAP_H

#include "Map.h"


template <typename K, typename V, class Hasher>
class LinearProbingHashMap : public Map<K, V> {
protected:
    typedef typename Map<K, V>::_self_t _base_t;
    typedef LinearProbingHashMap<K, V, Hasher> _self_t;
public:
    using typename _base_t::item;
    using typename _base_t::const_item;
    using typename _base_t::iterator;
    using typename _base_t::const_iterator;

protected:

    struct Cell {
        item *keyvalue = nullptr;
        bool used() const { return keyvalue != nullptr; }
        bool free() const { return keyvalue == nullptr; }
    };

    template<typename I>
    class _IteratorImpl : public _base_t::template MapIterator<I>::Impl {
        typedef typename _base_t::template MapIterator<I>::Impl _impl_base_t;

        Cell *table;
        size_t index;
        size_t tabSize;
        friend class LinearProbingHashMap;

        _IteratorImpl(Cell *table, size_t index, const size_t &tabSize)
                : table(table), index(index), tabSize(tabSize) {}

        _IteratorImpl<I>* copy() const { return new _IteratorImpl<I>(table, index, tabSize); }
        virtual void advance() {
            do {
                index++;
            } while (index < tabSize && table[index].free());
        };
        virtual bool operator==(const _impl_base_t& other) const {
            auto it = dynamic_cast<const _IteratorImpl*>(&other);
            if (it != nullptr) {
                if (table != it->table) return false;
                if (isEnd()) return it->isEnd();
                else return index == it->index;
            }
            return false;
        };
        virtual I& getValue() {
            return *table[index].keyvalue;
        };

        bool isEnd() const {
            return index >= tabSize;
        }

    };

    virtual size_t _getStep(const K&) const { return 1; }

public:

    LinearProbingHashMap(size_t capacity=256)
            : table(new Cell[capacity]), tabSize(capacity) {}

    virtual _self_t* copy() const {
        _self_t* copied = new _self_t(tabSize);
        for (int i = 0; i < tabSize; ++i) {
            if (table[i].used()) {
                copied->table[i].keyvalue = new item(*table[i].keyvalue);
            }
        }
        return copied;
    };
    virtual iterator find(const K& key) {
        size_t idx;
        if (!_find(key, idx)) return this->end();
        return iterator(new _IteratorImpl<item>(table, idx, tabSize));
    };
    virtual const_iterator find(const K& key) const {
        size_t idx;
        if (!_find(key, idx)) return this->end();
        return const_iterator(new _IteratorImpl<const_item>(table, idx, tabSize));
    };
    virtual bool put(const K& key, const V& val) {
        while(true) {
            size_t hash = hasher(key) % tabSize;
            size_t step = _getStep(key);
            size_t maxChain = tabSize / step;
            for (size_t stepId = 0; stepId < maxChain; ++stepId) {
                size_t i = (hash + step*stepId) % tabSize;
                if (table[i].used()) {
                    if (table[i].keyvalue->first == key) return false;
                    else continue;
                }
                table[i].keyvalue = new item(key, val);
                return true;
            }
            _rebuild(2 * tabSize);
        }
    };
    virtual void remove(const iterator& pos) {
        auto it = dynamic_cast<_IteratorImpl<item>*>(pos.getImpl());
        if (it == nullptr || it->isEnd()) throw std::runtime_error("wrong iterator");
        size_t index = it->index;
        it->advance();
        _destroyCell(index);
    };
    virtual size_t size() const {
        size_t result = 0;
        for (int i = 0; i < tabSize; ++i) {
            result += table[i].used();
        }
        return result;
    };
    virtual void clear() {
        for (size_t i = 0; i < tabSize; ++i) {
            if (table[i].used()) {
                _destroyCell(i);
            }
        }
    }

    virtual ~LinearProbingHashMap() {
        _destroyTable(table, tabSize);
    }

protected:

    bool _find(const K& key, size_t &result) const {
        size_t hash = size_t(hasher(key) % tabSize);
        size_t step = _getStep(key);
        size_t maxChain = tabSize / step;
        for (size_t stepId = 0; stepId < maxChain; ++stepId) {
            size_t i = (hash + step*stepId) % tabSize;
            if (table[i].free()) {
                return false;
            }
            if (key == table[i].keyvalue->first) {
                result = i;
                return true;
            }
        }
        return false;
    }
    void _rebuild(size_t newSize) {
        Cell *oldTable = table;
        size_t oldSize = tabSize;

        table = new Cell[newSize];
        tabSize = newSize;

        for (size_t i = 0; i < oldSize; ++i) {
            if (oldTable[i].free()) continue;
            put(oldTable[i].keyvalue->first, oldTable[i].keyvalue->second);
        }
        _destroyTable(oldTable, oldSize);
    }
    void _destroyTable(Cell* t, size_t sz) {
        for (size_t i = 0; i < sz; ++i) {
            if (t[i].used()) {
                delete t[i].keyvalue;
            }
        }
        delete[] t;
    }
    void _destroyCell(size_t index) {
        delete table[index].keyvalue;
        table[index].keyvalue = nullptr;
    }

    Hasher hasher = Hasher();
    Cell *table;
    size_t tabSize;


    virtual _IteratorImpl<item>* _begin_impl() {
        auto it = new _IteratorImpl<item>(
                table, 0, tabSize
        );
        it->advance();
        return it;
    };
    virtual _IteratorImpl<item>* _end_impl() {
        return new _IteratorImpl<item>(
                table, tabSize, tabSize
        );
    };
    virtual _IteratorImpl<const_item>* _cbegin_impl() const {
        auto it = new _IteratorImpl<const_item>(
                const_cast<Cell*>(table), 0, tabSize
        );
        it->advance();
        return it;
    };
    virtual _IteratorImpl<const_item>* _cend_impl() const {
        return new _IteratorImpl<const_item>(
                const_cast<Cell*>(table), tabSize, tabSize
        );
    };
};


#endif //NUMMETH_LINEARPROBINGHASHMAP_H
