#ifndef NUMMETH_HASHMAP_H
#define NUMMETH_HASHMAP_H

#include <tuple>


#include "../common/ForwardIterator.h"


template<typename K, typename V>
class Map {
protected:
    template<typename I> class MapIterator;
public:

    typedef std::pair<K, V> item;
    typedef const std::pair<K, V> const_item;

    typedef MapIterator<item> iterator;
    typedef MapIterator<const_item> const_iterator;

protected:
    typedef Map<K, V> _self_t;

    template<typename I>
    class MapIterator: public ForwardIterator<I> {
        friend class Map;
        typedef ForwardIterator<I> _base_t;
    public:
        using typename _base_t::Impl;
        using _base_t::getImpl;

        MapIterator(const _base_t& other): _base_t(other) {}
        MapIterator(Impl* impl): _base_t(impl) {}
    };

    virtual typename MapIterator<item>::Impl* _begin_impl() = 0;
    virtual typename MapIterator<item>::Impl* _end_impl() = 0;
    virtual typename MapIterator<const_item>::Impl* _cbegin_impl() const = 0;
    virtual typename MapIterator<const_item>::Impl* _cend_impl() const = 0;

    typename MapIterator<item>::Impl* _get_impl(MapIterator<item>& it) { return it.getImpl(); }


public:

    virtual _self_t* copy() const = 0;
    virtual iterator find(const K&) = 0;
    virtual const_iterator find(const K&) const = 0;
    virtual bool put(const K&, const V&) = 0;
    virtual void remove(const iterator&) = 0;
    virtual size_t size() const = 0;
    virtual void clear() = 0;

    bool remove(const K& k) {
        iterator pos = find(k);
        if (pos != end()) {
            remove(pos);
            return true;
        } else {
            return false;
        }
    }
    bool contains(const K& k) const {
        return find(k) != end();
    }
    V& operator[](const K& k) {
        iterator pos = find(k);
        if (pos == this->end()) throw std::out_of_range("not found");
        return pos->second;
    };
    const V& operator[](const K& k) const {
        iterator pos = find(k);
        if (pos == this->end()) throw std::out_of_range("not found");
        return pos->second;
    };

    iterator begin() {
        return MapIterator<item>(_begin_impl());
    }
    iterator end() {
        return MapIterator<item>(_end_impl());
    }
    const_iterator begin() const {
        return MapIterator<const_item>(_cbegin_impl());
    }
    const_iterator end() const {
        return MapIterator<const_item>(_cend_impl());
    }

    virtual ~Map() {}

};


template<typename K, typename V>
std::ostream& operator<<(std::ostream& stream, const Map<K, V>& map) {
    stream << "{";
    auto it = map.begin();
    if (it != map.end()) {
        stream << it->first << ": " << it->second;
        it++;
    }
    for(; it != map.end(); ++it) {
        stream << "; " << it->first << ": " << it->second;
    }
    stream << "}";
    return stream;
};


#endif //NUMMETH_HASHMAP_H
