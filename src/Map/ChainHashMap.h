#ifndef NUMMETH_CHAINHASHMAP_H
#define NUMMETH_CHAINHASHMAP_H

#include "Map.h"
#include "../common/LinkedList.h"
#include "../common/ForwardIterator.h"


template <typename K, typename V, class Hasher, size_t TABLE_SIZE=256>
class ChainHashMap : public Map<K, V> {
private:
    typedef typename Map<K, V>::_self_t _base_t;
    typedef ChainHashMap<K, V, Hasher, TABLE_SIZE> _self_t;
public:
    using typename _base_t::item;
    using typename _base_t::const_item;
    using typename _base_t::iterator;
    using typename _base_t::const_iterator;

private:

    typedef typename LinkedList<item>::Node node_t;
    typedef typename LinkedList<const_item>::Node const_node_t;

    template<typename I>
    class _IteratorImpl : public _base_t::template MapIterator<I>::Impl {
        typedef typename _base_t::template MapIterator<I>::Impl _impl_base_t;
        typedef typename LinkedList<item>::Node _iter_node_t;

        LinkedList<item> *table;
        size_t index;
        _iter_node_t * node;
        friend class ChainHashMap;

        _IteratorImpl(LinkedList<item> *table, size_t index, _iter_node_t* node)
                : table(table), index(index), node(node) {}

        _IteratorImpl<I>* copy() const { return new _IteratorImpl<I>(table, index, node); }
        virtual void advance() {
            if (node != nullptr && node->next != nullptr) {
                node = node->next;
                return;
            }
            do {
                index++;
            } while (index < TABLE_SIZE && table[index].head == nullptr);
            if (index < TABLE_SIZE) {
                node = table[index].head;
            } else {
                node = nullptr;
            }
        };
        virtual bool operator==(const _impl_base_t& other) const {
            auto it = dynamic_cast<const _IteratorImpl*>(&other);
            if (it != nullptr) {
                return table == it->table && index == it->index && node == it->node;
            }
            return false;
        };
        virtual I& getValue() {
            return node->data;
        };

    };

public:

    ChainHashMap() {}

    virtual _self_t* copy() const {
        _self_t* copied = new _self_t;
        for (int i = 0; i < TABLE_SIZE; ++i) {
            if (table[i].head != nullptr) {
                copied->table[i].head = new node_t(table[i].head->data);
                copied->table[i].size = table[i].size;
                node_t* src = table[i].head->next;
                node_t* last_dst = copied->table[i].head;
                while (src != nullptr) {
                    copied->table[i].insertAfter(new node_t(src->data), last_dst);
                    last_dst = last_dst->next;
                    src = src->next;
                }
            }
        }
        return copied;
    };
    virtual iterator find(const K& key) {
        node_t *n;
        size_t idx;
        if (!_find(key, idx, n)) return this->end();
        return iterator(new _IteratorImpl<item>(table, idx, n));
    };
    virtual const_iterator find(const K& key) const {
        node_t *n;
        size_t idx;
        if (!_find(key, idx, n)) return this->end();
        return const_iterator(new _IteratorImpl<const_item>(
                const_cast<LinkedList<item>*>(table), idx, n));
    };
    virtual bool put(const K& key, const V& val) {
        size_t index = hasher(key) % TABLE_SIZE;
        auto &lst = table[index];
        if (lst.head == nullptr) {
            lst.head = new node_t(item(key, val));
            lst.size = 1;
            return true;
        } else {
            node_t *n = lst.head;
            while (n != nullptr) {
                if (n->data.first == key) return false;
                n = n->next;
            }
            lst.insertBefore(new node_t(item(key, val)), lst.head);
            return true;
        }
    };
    virtual void remove(const iterator& pos) {
        auto it = dynamic_cast<_IteratorImpl<item>*>(pos.getImpl());
        if (it == nullptr || it->node == nullptr) throw std::runtime_error("wrong iterator");
        size_t index = it->index;
        node_t *node = it->node;
        it->advance();
        table[index].remove(node);
    };
    virtual size_t size() const {
        size_t result = 0;
        for (int i = 0; i < TABLE_SIZE; ++i) {
            result += table[i].size;
        }
        return result;
    };
    virtual void clear() {
        for (int i = 0; i < TABLE_SIZE; ++i) {
            table[i].clear();
        }
    }

private:

    bool _find(const K& key, size_t &index, node_t *&node) const {
        index = size_t(hasher(key) % TABLE_SIZE);
        node = table[index].head;
        while (node != nullptr && node->data.first != key) {
            node = node->next;
        }
        return node != nullptr;
    }

    Hasher hasher = Hasher();
    LinkedList<item> table[TABLE_SIZE] = {};


    virtual _IteratorImpl<item>* _begin_impl() {
        auto it = new _IteratorImpl<item>(
                table, 0, table[0].head
        );
        it->advance();
        return it;
    };
    virtual _IteratorImpl<item>* _end_impl() {
        return new _IteratorImpl<item>(
                table, TABLE_SIZE, nullptr
        );
    };
    virtual _IteratorImpl<const_item>* _cbegin_impl() const {
        auto it = new _IteratorImpl<const_item>(
                const_cast<LinkedList<item>*>(table), 0, table[0].head
        );
        it->advance();
        return it;
    };
    virtual _IteratorImpl<const_item>* _cend_impl() const {
        return new _IteratorImpl<const_item>(
                const_cast<LinkedList<item>*>(table), TABLE_SIZE, nullptr
        );
    };

};


#endif //NUMMETH_CHAINHASHMAP_H
