#ifndef NUMMETH_DOUBLEHASHINGMAP_H
#define NUMMETH_DOUBLEHASHINGMAP_H

#include "LinearProbingHashMap.h"


template <typename K, typename V, class Hasher, class Hasher2>
class DoubleHashingMap : public LinearProbingHashMap<K, V, Hasher> {
private:
    typedef typename LinearProbingHashMap<K, V, Hasher>::_self_t _base_t;
    typedef DoubleHashingMap<K, V, Hasher, Hasher2> _self_t;
public:
    using typename _base_t::item;
    using typename _base_t::const_item;
    using typename _base_t::iterator;
    using typename _base_t::const_iterator;

    DoubleHashingMap(size_t capacity=256): _base_t(capacity) {}

protected:
    using _base_t::tabSize;
    using _base_t::table;

    Hasher2 hasher2 = Hasher2();

    virtual size_t _getStep(const K& key) const {
        size_t hash = hasher2(key);
        size_t step = hash % 97;
        if (step % 2 == 0) step++;
        return step;
    }

public:
    virtual _self_t* copy() const {
        _self_t* copied = new _self_t(tabSize);
        for (int i = 0; i < tabSize; ++i) {
            if (table[i].used()) {
                copied->table[i].keyvalue = new item(*table[i].keyvalue);
            }
        }
        return copied;
    };

};

#endif //NUMMETH_DOUBLEHASHINGMAP_H
