#ifndef NUMMETH_KEY_H
#define NUMMETH_KEY_H

#include <iostream>


class Key {
    int *_data;
    size_t _size;

public:

    Key(): _data(nullptr), _size(0) {}
    Key(const Key& k) : _data(new int[k._size]), _size(k._size) {
        for (int i = 0; i < k._size; ++i) {
            _data[i] = k._data[i];
        }
    }


    template<typename Iterator>
    Key(Iterator begin, Iterator end): _data(nullptr), _size(0) {
        _size = end - begin;
        _data = new int[_size];
        int* dst = _data;
        for(auto it = begin; it != end; it++, dst++) {
            *dst = *it;
        }
    }

    Key(std::initializer_list<int> data): Key(data.begin(), data.end()) {}

    int* data() { return _data; }
    int& operator[](size_t idx) { return _data[idx]; }
    int operator[](size_t idx) const { return _data[idx]; }
    size_t size() const { return _size; }

    bool operator==(const Key& k) const {
        if (_data == k._data) return true;
        if (_size != k._size) return false;
        for (int i = 0; i < _size; ++i) {
            if (_data[i] != k._data[i]) return false;
        }
        return true;
    }
    bool operator!=(const Key& k) const {
        return !(*this == k);
    }

    virtual ~Key() { if (_data != nullptr) delete[] _data; }
};


struct ModHasher {

    unsigned operator()(const Key& key) const;

};


struct SubstHasher {

    unsigned operator()(const Key& key) const;

private:
    static const unsigned char substTable[256];
};


std::ostream& operator<<(std::ostream& stream, const Key& key);


#endif //NUMMETH_KEY_H
