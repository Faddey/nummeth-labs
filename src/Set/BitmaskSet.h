#ifndef NUMMETH_BITMASKSET_H
#define NUMMETH_BITMASKSET_H

#include "Set.h"


template <typename T>
class BitmaskSet : public Set<T> {

    typedef typename Set<T>::_self_t _base_t;
    typedef BitmaskSet<T> _self_t;

    const _self_t* _as_self(const _base_t& s) const {
        auto _s = dynamic_cast<const _self_t*>(&s);
        if (_s && *u != *_s->u) _s = nullptr;
        return _s;
    }

    static uint64_t _bit2mask(int bit) { return uint64_t(1) << (bit); }

    template<typename V, typename U>
    class _IteratorImpl : public _base_t::template SetIterator<V>::Impl {

        const BitmaskSet<U>* set;
        int bit;
        friend class BitmaskSet;

    public:
        _IteratorImpl(const BitmaskSet<U>* set, int bit = 0): set(set), bit(bit) {}

        _IteratorImpl<V, U>* copy() const { return new _IteratorImpl<V, U>(set, bit); }
        virtual void advance() {
            do {
                bit++;
            } while (((_bit2mask(bit) & set->mask) == 0) && (bit < set->u->n_elements));
        };
        virtual bool operator==(const typename _base_t::template SetIterator<V>::Impl& other) const {
            auto it = dynamic_cast<const _IteratorImpl*>(&other);
            if (it != nullptr) {
                return set == it->set && bit == it->bit;
            }
            return false;
        };
        virtual V& getValue() {
            return const_cast<V&>(set->u->elements[bit]);
        };

    };

    struct _Universum {
        const size_t n_elements;
        const T* const elements;
        unsigned refcount = 1;

        _Universum(const size_t n_elements, const T* elements)
                : n_elements(n_elements), elements(elements) {}

        bool operator==(const _Universum& u) const {
            if (this == &u) return true;
            if (n_elements != u.n_elements) return false;
            for (int i = 0; i < n_elements; ++i) {
                if (elements[i] != u.elements[i]) return false;
            }
            return true;
        }

        bool operator!=(const _Universum& u) const {
            return !(*this == u);
        }

        int findBit(const T& e) {
            int bit = tryFindBit(e);
            if (bit == -1) throw std::runtime_error("no such element in the universum");
            return bit;
        }

        int tryFindBit(const T& e) {
            for (int i = 0; i < n_elements; ++i) {
                if (e == elements[i]) return i;
            }
            return -1;
        }

        ~_Universum() {
            if (elements != nullptr) delete[] elements;
        }
    };

    static const int MAX_BITS = 64;
    _Universum *u;
    uint64_t mask;

    BitmaskSet(_Universum *u, uint64_t mask) : u(u), mask(mask) {}

public:

    template <class _Iterator>
    BitmaskSet(size_t n_elements, _Iterator begin, _Iterator end)
            : u(nullptr), mask(0) {
        if (n_elements > MAX_BITS) {
            throw std::runtime_error("too many elements for bitmask set");
        }
        T* buffer = new T[n_elements];
        T* dst = buffer;
        for (auto it = begin; it != end; it++, dst++) {
            *dst = *it;
        }
        u = new _Universum(n_elements, buffer);
        mask = _bit2mask(n_elements) - 1;
    }

    template <class _Iterator>
    BitmaskSet(_Iterator begin, _Iterator end)
            : BitmaskSet(end-begin, begin, end) {}

    BitmaskSet(std::initializer_list<T> elems) : BitmaskSet(elems.begin(), elems.end()) {}

    virtual _self_t* copy() const {
        auto cp = new BitmaskSet<T>(const_cast<_Universum*>(u), mask);
        cp->u->refcount++;
        return cp;
    }
    virtual bool add(const T& e) {
        uint64_t e_mask = _bit2mask(u->findBit(e));
        if (mask & e_mask) {
            return false;
        } else {
            mask |= e_mask;
            return true;
        }
    }
    virtual bool remove(const T& e) {
        int bit = u->tryFindBit(e);
        if (bit == -1) return false;
        uint64_t e_mask = _bit2mask(bit);
        if (mask & e_mask) {
            mask &= ~e_mask;
            return true;
        } else {
            return false;
        }
    }
    virtual void remove(typename _base_t::iterator& it) {
        _IteratorImpl<T, T> *_it = dynamic_cast<_IteratorImpl<T, T>*>(_base_t::_get_impl(it));
        if (_it == nullptr || _it->set != this || _it->bit >= u->n_elements) {
            throw std::runtime_error("wrong iterator");
        }
        int bit = _it->bit;
        _it->advance();
        mask &= ~_bit2mask(bit);
    }
    virtual bool contains(const T& e) const {
        int bit = u->tryFindBit(e);
        if (bit == -1) return false;
        return (mask & _bit2mask(bit)) != 0;
    }
    virtual size_t size() const {
        size_t sz = 0;
        uint64_t msk = mask;
        while(msk) {
            sz += (msk % 2);
            msk >>= 1;
        }
        return sz;
    }
    virtual void clear()  {
        mask = 0;
    }
    virtual void update(const _base_t& s) {
        auto _s = _as_self(s);
        if (!_s) _base_t::update(s);
        else mask |= _s->mask;
    };
    virtual void intersect(const _base_t& s) {
        auto _s = _as_self(s);
        if (!_s) _base_t::intersect(s);
        else mask &= _s->mask;
    }
    virtual void subtract(const _base_t& s) {
        auto _s = _as_self(s);
        if (!_s) _base_t::subtract(s);
        else mask &= ~_s->mask;
    };
    virtual void symmetric_difference(const _base_t &s) {
        auto _s = _as_self(s);
        if (!_s) _base_t::symmetric_difference(s);
        else mask ^= _s->mask;
    }

    void invert() {
        mask = ~mask;
    }
    void setMask(uint64_t mask) {
        this->mask = mask;
    }
    size_t universumSize() const {
        return u->n_elements;
    }
    void setToUniversum() {
        clear();
        invert();
    }

    virtual ~BitmaskSet() {
        if (u != nullptr) {
            u->refcount--;
            if (u->refcount == 0) {
                delete u;
            }
        }
    }

private:

    virtual typename _base_t::template SetIterator<T>::Impl* _begin_impl() {
        auto it = new _IteratorImpl<T, T>(this, -1);
        it->advance();
        return it;
    };
    virtual typename _base_t::template SetIterator<T>::Impl* _end_impl() {
        return new _IteratorImpl<T, T>(this, u->n_elements);
    };
    virtual typename _base_t::template SetIterator<const T>::Impl* _cbegin_impl() const {
        auto it = new _IteratorImpl<const T, T>(this, -1);
        it->advance();
        return it;
    };
    virtual typename _base_t::template SetIterator<const T>::Impl* _cend_impl() const {
        return new _IteratorImpl<const T, T>(this, u->n_elements);
    };

};

#endif //NUMMETH_BITMASKSET_H
