#ifndef NUMMETH_LINKEDLISTSET_H
#define NUMMETH_LINKEDLISTSET_H

#include <stdexcept>
#include "Set.h"
#include "../common/LinkedList.h"


template <typename T, class Compare=std::less<T>>
class LinkedListSet : public Set<T> {
    typedef typename Set<T>::_self_t _base_t;
    typedef LinkedListSet<T> _self_t;
    typedef typename LinkedList<T>::Node node_t;

    static const _self_t* _as_self(const _base_t& s) {
        return dynamic_cast<const _self_t*>(&s);
    }

    template<typename V>
    class _IteratorImpl : public _base_t::template SetIterator<V>::Impl {

        node_t* node;
        friend class LinkedListSet;

        _IteratorImpl(node_t* node): node(node) {}

        _IteratorImpl<V>* copy() const { return new _IteratorImpl<V>(node); }
        virtual void advance() {
            node = node->next;
        };
        virtual bool operator==(const typename _base_t::template SetIterator<V>::Impl& other) const {
            auto it = dynamic_cast<const _IteratorImpl*>(&other);
            if (it != nullptr) {
                return node == it->node;
            }
            return false;
        };
        virtual V& getValue() {
            return node->data;
        };

    };

    LinkedList<T> lst;
    Compare less;

public:

    LinkedListSet(): lst(nullptr, 0), less() {}
    LinkedListSet(std::initializer_list<T> data): lst(nullptr, 0), less() {
        for(const T& e : data) {
            add(e);
        }
    }
    LinkedListSet(const Set<T>& set): lst(nullptr, 0), less() {
        update(set);
    }
    LinkedListSet(const LinkedListSet<T>& set): lst(nullptr, 0), less() {
        update(set);
    }

    virtual _self_t* copy() const {
        return new _self_t(*this);
    }
    virtual bool add(const T& e) {
        if (lst.head == nullptr) {
            lst.head = new node_t(e);
            lst.size = 1;
            return true;
        }
        node_t* n = lst.head;
        bool greater_found = less(e, lst.head->data);
        while (!greater_found && n->next) {
            n = n->next;
            greater_found = less(e, n->data);
        }
        if (greater_found) {
            if (n->prev && e == n->prev->data) {
                return false;
            }
            lst.insertBefore(new node_t(e), n);
        } else {
            lst.insertAfter(new node_t(e), n);
        }
        return true;
    }
    virtual bool remove(const T& e) {
        node_t* n = _findByValue(e);
        if (n != nullptr) {
            lst.remove(n);
            return true;
        }
        return false;
    }
    virtual void remove(typename _base_t::iterator& it) {
        _IteratorImpl<T> *_it = dynamic_cast<_IteratorImpl<T>*>(_base_t::_get_impl(it));
        if (_it == nullptr || _it->node == nullptr) {
            throw std::runtime_error("wrong iterator");
        }
        node_t *n = _it->node;
        _it->node = n->next;
        lst.remove(n);
    }
    virtual bool contains(const T& e) const {
        return _findByValue(e) != nullptr;
    }
    virtual size_t size() const {
        return lst.size;
    }
    virtual void clear()  {
        lst.clear();
    }
    virtual void update(const _base_t& s) {
        auto _s = _as_self(s);
        if (!_s) _base_t::update(s);
        else _union_inplace(_s->lst);
    };
    virtual _base_t* union_(const _base_t& s1, const _base_t& s2) {
        auto _s1 = _as_self(s1);
        auto _s2 = _as_self(s2);
        if (!_s1 && !_s2) return _base_t::union_(s1, s2);
        else if (_s1 && _s2) {
            node_t *tail = _copyFrom(_s1->lst);
            if (tail) lst.clear(tail);
            _union_inplace(_s2->lst);
        } else {
            auto lls = _s1 ? _s1 : _s2;
            if (lls == _s1) this->assign(s2);
            else this->assign(s1);
            _union_inplace(lls->lst);
        };
        return this;
    }
    virtual void intersect(const _base_t& s) {
        auto _s = _as_self(s);
        if (_s) _intersect_inplace(_s->lst);
        else _base_t::intersect(s);
    }
    virtual _base_t* intersection(const _base_t& s1, const _base_t& s2) {
        auto _s1 = _as_self(s1);
        auto _s2 = _as_self(s2);
        if (!_s1 || !_s2) return _base_t::intersection(s1, s2);
        else _put_intersection(_s1->lst, _s2->lst);
        return this;
    }
    virtual void subtract(const _base_t& s) {
        auto _s = _as_self(s);
        if (_s) _subtract_inplace(_s->lst);
        else _base_t::subtract(s);
    };
    virtual _base_t* subtraction(const _base_t& s1, const _base_t& s2) {
        auto _s1 = _as_self(s1);
        auto _s2 = _as_self(s2);
        if (!_s1 || !_s2) return _base_t::subtraction(s1, s2);
        else _put_subtraction(_s1->lst, _s2->lst);
        return this;
    }
    virtual void symmetric_difference(const _base_t &s) {
        auto _s = _as_self(s);
        if (_s) _symmetric_difference_inplace(_s->lst);
        else _base_t::symmetric_difference(s);
    }
    virtual _base_t* symmetric_difference(const _base_t &s1, const _base_t& s2) {
        auto _s1 = _as_self(s1);
        auto _s2 = _as_self(s2);
        if (!_s1 || !_s2) return _base_t::symmetric_difference(s1, s2);
        else _put_symmetric_difference(_s1->lst, _s2->lst);
        return this;
    }

    virtual ~LinkedListSet() {
        clear();
    }

private:
    void _union_inplace(const LinkedList<T>& l) {
        if (lst.head == nullptr) {
            _copyFrom(l);
            return;
        }
        if (l.head == nullptr) { return; }
        if (lst.head == l.head) { return; }
        node_t *our = lst.head, *their = l.head;
        // invariants:
        // all before `their` are already inserted
        // `our->prev` <= their
        while (their != nullptr) {
            if (less(their->data, our->data)) {
                lst.insertBefore(new node_t(their->data), our);
                their = their->next;
            } else {
                if (our->next == nullptr) { break; }
                our = our->next;
            }
        }
        // we get into this loop only if `our` is last and
        // there are more `theirs` that are greater than `our`
        while (their != nullptr) {
            lst.insertAfter(new node_t(their->data), our);
            our = our->next;
            their = their->next;
        }
    }
    void _intersect_inplace(const LinkedList<T>& l) {
        if (lst.head == nullptr) { return; }
        if (l.head == nullptr) {
            lst.clear();
            return;
        }
        size_t new_size = 0;
        node_t *our = lst.head, *their = l.head;
        while (our != nullptr && their != nullptr) {
            if (less(our->data, their->data)) {
                our = lst.remove(our);
            } else {
                if (our->data == their->data) {
                    our = our->next;
                    new_size++;
                }
                their = their->next;
            }
        }
        if (our) lst.clear(our);
        lst.size = new_size;
    }
    void _put_intersection(const LinkedList<T>& l1, const LinkedList<T>& l2) {
        node_t *dst = lst.head, *prev = nullptr, *n1 = l1.head, *n2 = l2.head;
        size_t new_size = 0;
        while (n1 != nullptr && n2 != nullptr) {
            if (n1->data == n2->data) {
                new_size++;
                _putAndMaintainPointers(dst, prev, n1->data);
                n1 = n1->next;
                n2 = n2->next;
            } else {
                if (less(n1->data, n2->data)) {
                    n1 = n1->next;
                } else {
                    n2 = n2->next;
                }
            }
        }
        if (dst) lst.clear(dst);
        lst.size = new_size;
    }
    void _subtract_inplace(const LinkedList<T>& l) {
        node_t *our = lst.head, *their = l.head;
        while (our != nullptr && their != nullptr) {
            if (less(our->data, their->data)) {
                our = our->next;
            } else {
                if (our->data == their->data) {
                    our = lst.remove(our);
                }
                their = their->next;
            }
        }
    }
    void _put_subtraction(const LinkedList<T>& l_minuend, const LinkedList<T>& l_subtrahend) {
        node_t *dst = lst.head, *prev = nullptr,
             *minuend = l_minuend.head, *subtrahend = l_subtrahend.head;
        size_t new_size = 0;
        while (minuend != nullptr && subtrahend != nullptr) {
            if (less(minuend->data, subtrahend->data)) {
                new_size++;
                _putAndMaintainPointers(dst, prev, minuend->data);
                minuend = minuend->next;
            } else if (minuend->data == subtrahend->data) {
                minuend = minuend->next;
                subtrahend = subtrahend->next;
            } else {
                subtrahend = subtrahend->next;
            }
        }
        while(minuend != nullptr) {
            new_size++;
            _putAndMaintainPointers(dst, prev, minuend->data);
            minuend = minuend->next;
        }
        if (dst) lst.clear(dst);
        lst.size = new_size;
    }
    void _symmetric_difference_inplace(const LinkedList<T>& l) {
        node_t *our = lst.head, *last = nullptr, *their = l.head;
        while (our != nullptr && their != nullptr) {
            if (less(our->data, their->data)) {
                last = our;
                our = our->next;
            } else if (our->data == their->data) {
                last = our;
                our = lst.remove(our);
                their = their->next;
            } else {
                lst.insertBefore(new node_t(their->data), our);
                their = their->next;
            }
        }
        if (their) {
            if (last == nullptr) {
                _copyFrom(l);
            } else {
                while (their != nullptr) {
                    lst.insertAfter(new node_t(their->data), last);
                    last = last->next;
                    their = their->next;
                }
            }
        }
    }
    void _put_symmetric_difference(const LinkedList<T>& l1, const LinkedList<T>& l2) {
        node_t *dst = lst.head, *prev = nullptr, *n1 = l1.head, *n2 = l2.head;
        size_t new_size = 0;
        while (n1 != nullptr && n2 != nullptr) {
            if (less(n1->data, n2->data)) {
                new_size++;
                _putAndMaintainPointers(dst, prev, n1->data);
                n1 = n1->next;
            } else if (n1->data == n2->data) {
                n1 = n1->next;
                n2 = n2->next;
            } else {
                new_size++;
                _putAndMaintainPointers(dst, prev, n2->data);
                n2 = n2->next;
            }
        }
        while (n1 != nullptr) {
            new_size++;
            _putAndMaintainPointers(dst, prev, n1->data);
            n1 = n1->next;
        }
        while (n2 != nullptr) {
            new_size++;
            _putAndMaintainPointers(dst, prev, n2->data);
            n2 = n2->next;
        }
        if (dst) lst.clear(dst);
        lst.size = new_size;
    }
    node_t* _copyFrom(const LinkedList<T>& l) {
        // returns node after end. sets size to l.size.
        if (l.head == nullptr) {
            return lst.head;
        } else {
            if (lst.head == nullptr) {
                lst.head = new node_t(l.head->data);
            } else {
                lst.head->data = l.head->data;
            }
        }
        node_t *dst = lst.head->next, *prev = lst.head, *src = l.head->next;
        while (src != nullptr) {
            if (dst == nullptr) {
                dst = new node_t(src->data);
                lst.insertAfter(dst, prev);
            } else {
                dst->data = src->data;
            }
            prev = dst;
            dst = dst->next;
            src = src->next;
        }
        lst.size = l.size;
        return dst;
    }

    node_t* _findByValue(const T& e) const {
        node_t* n = lst.head;
        while (n != nullptr) {
            if (n->data == e) {
                return n;
            }
            n = n->next;
        }
        return nullptr;
    }

    void _putAndMaintainPointers(node_t *&dst, node_t *&prev, const T &e) {
        // do common logic for adding an item during replacing operations
        // dst - pointer to next probably existing node for writing
        // prev - pointer to previous node, where we wrote last time
        // e - data to write
        // if both dst and prev are nulls, we are empty, so create head
        // if dst is null and prev is not, we are appending to the end
        // if dst is not null, we are reusing existing node, so simply update it
        if (dst == nullptr) {
            dst = new node_t(e);
            if (prev == nullptr) {
                lst.head = dst;
            } else {
                lst.insertAfter(dst, prev);
            }
        } else {
            dst->data = e;
        }
        prev = dst;
        dst = dst->next;
    }

    virtual typename _base_t::template SetIterator<T>::Impl* _begin_impl() {
        return new _IteratorImpl<T>(lst.head);
    };
    virtual typename _base_t::template SetIterator<T>::Impl* _end_impl() {
        return new _IteratorImpl<T>(nullptr);
    };
    virtual typename _base_t::template SetIterator<const T>::Impl* _cbegin_impl() const {
        return new _IteratorImpl<const T>(lst.head);
    };
    virtual typename _base_t::template SetIterator<const T>::Impl* _cend_impl() const {
        return new _IteratorImpl<const T>(nullptr);
    };

};

#endif //NUMMETH_LINKEDLISTSET_H
