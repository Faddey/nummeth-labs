#ifndef NUMMETH_SET_H
#define NUMMETH_SET_H


#include <cstdlib>
#include <ios>
#include <iterator>

#include "../common/ForwardIterator.h"

template<typename T>
class Set {
protected:
    typedef Set<T> _self_t;

    template<typename V>
    class SetIterator: public ForwardIterator<V> {
        friend class Set;
        typedef ForwardIterator<V> _base_t;
    public:
        using typename _base_t::Impl;
        using _base_t::getImpl;

        SetIterator(const _base_t& other): _base_t(other) {}
        SetIterator(Impl* impl): _base_t(impl) {}
    };

    virtual typename SetIterator<T>::Impl* _begin_impl() = 0;
    virtual typename SetIterator<T>::Impl* _end_impl() = 0;
    virtual typename SetIterator<const T>::Impl* _cbegin_impl() const = 0;
    virtual typename SetIterator<const T>::Impl* _cend_impl() const = 0;

    typename SetIterator<T>::Impl* _get_impl(SetIterator<T>& it) { return it.getImpl(); }

public:
    typedef SetIterator<T> iterator;
    typedef SetIterator<const T> const_iterator;

    virtual _self_t* copy() const = 0;
    virtual bool add(const T& e) = 0;
    virtual bool remove(const T& e) = 0;
    virtual void remove(iterator& it) = 0;
    virtual bool contains(const T& e) const = 0;
    virtual size_t size() const = 0;
    virtual void clear() = 0;
    virtual void assign(const _self_t& s) {
        clear();
        update(s);
    }
    virtual void assign(std::initializer_list<T> items) {
        clear();
        update(items);
    }
    virtual void update(std::initializer_list<T> items) {
        for (const T& e : items) {
            add(e);
        }
    }
    virtual void update(const _self_t& s) {
        for (const T& e : s) {
            add(e);
        }
    }
    virtual _self_t* union_(const _self_t& s1, const _self_t& s2) {
        assign(s1);
        update(s2);
        return this;
    }
    virtual void subtract(const _self_t& s) {
        for (const T& e : s) {
            remove(e);
        }
    }
    virtual _self_t* subtraction(const _self_t& s1, const _self_t& s2) {
        assign(s1);
        subtract(s2);
        return this;
    };
    virtual void intersect(const _self_t& s) {
        for (iterator i = begin(); i != end(); ) {
            if (!s.contains(*i)) {
                remove(i);
            } else {
                std::next(i);
            }
        }
    }
    virtual _self_t* intersection(const _self_t& s1, const _self_t& s2) {
        assign(s1);
        intersect(s2);
        return this;
    };
    virtual void symmetric_difference(const _self_t &s) {
        for (const T& e : s) {
            if (!remove(e)) add(e);
        }
    }
    virtual _self_t* symmetric_difference(const _self_t &s1, const _self_t& s2) {
        assign(s1);
        symmetric_difference(s2);
        return this;
    };

    iterator begin() {
        return SetIterator<T>(_begin_impl());
    }
    iterator end() {
        return SetIterator<T>(_end_impl());
    }
    const_iterator begin() const {
        return SetIterator<const T>(_cbegin_impl());
    }
    const_iterator end() const {
        return SetIterator<const T>(_cend_impl());
    }

    virtual ~Set() {}

};


template<typename T>
std::ostream& operator<<(std::ostream& stream, const Set<T>& set) {
    stream << '{';
    auto it = set.begin();
    if (it != set.end()) {
        stream << *it;
        it++;
    }
    for(; it != set.end(); it++) {
        stream << ", ";
        stream << *it;
    }
    stream << '}';
    return stream;
}


#endif //NUMMETH_SET_H
