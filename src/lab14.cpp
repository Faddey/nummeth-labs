#include <iostream>
#include <string>
#include "Set/BitmaskSet.h"

using namespace std;


int main(int argc, char** argv) {
    BitmaskSet<string> set{"alice", "bob", "clark", "doris"};
    set.setToUniversum();

    cout << "Universum:" << endl << set << endl;
    cout << "Subsets in decimal order:" << endl;
    for(uint64_t mask = 0; mask < (1 << set.universumSize()); mask++) {
        set.setMask(mask);
        cout << set << endl;
    }
    cout << "Subsets in Gray code order:" << endl;
    for(unsigned i = 0; i < (1 << set.universumSize()); i++) {
        uint64_t mask = i ^ (i >> 1);
        set.setMask(mask);
        cout << set << endl;
    }
}
