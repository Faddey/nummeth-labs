#include <iostream>
#include "Map/ChainHashMap.h"
#include "Map/LinearProbingHashMap.h"
#include "Map/DoubleHashingMap.h"
#include "lab22/key.h"


using namespace std;



template <class Hasher>
void testMap(Map<Key, string>& M, const Key&, const Key&, Hasher);


int main(int argc, char** argv) {

    cout << "====================================================" << endl;
    cout << "================== Chain hash map ==================" << endl;
    cout << "====================================================" << endl;
    ChainHashMap<Key, string, ModHasher> M1;
    testMap<ModHasher>(M1, {101}, {702}, ModHasher());

    cout << endl << endl;

    cout << "====================================================" << endl;
    cout << "============== Linear probing hash map =============" << endl;
    cout << "====================================================" << endl;
    LinearProbingHashMap<Key, string, SubstHasher> M2;
    testMap<SubstHasher>(M2, {12, 43}, {15, 0}, SubstHasher());

    cout << endl << endl;

    cout << "====================================================" << endl;
    cout << "================= Double hashing map ===============" << endl;
    cout << "====================================================" << endl;
    DoubleHashingMap<Key, string, SubstHasher, ModHasher> M3;
    testMap<SubstHasher>(M3, {12, 43}, {15, 0}, SubstHasher());
}


template <class Hasher>
void testMap(Map<Key, string> & M, const Key& k1, const Key& k2, Hasher hasher) {
    M.clear();
    M.put({1, 2, 3}, "one two three");
    M.put({380, 95, 589, 61, 49}, "my phone");
    M.put({1, 2, 3, 2, 1}, "palindrome");
    cout << "M = " << M << endl;
    cout << "size() -> " << M.size() << endl;

    {
        auto copiedM = M.copy();
        cout << "copy of M = " << *copiedM << endl << endl;
        delete copiedM;
    }

    {
        auto pos = M.find({1, 2, 3});
        cout << "Found by key <1, 2, 3>: key="
             << pos->first << ", value=" << pos->second << endl << endl;
    }

    {
        cout << "Collision handling:" << endl;
        cout << "hash(" << k1 << ") -> " << hasher(k1) << endl;
        cout << "hash(" << k2 << ") -> " << hasher(k2) << endl;
        M.put(k1, "collision1");
        M.put(k2, "collision2");
        cout << "M = " << M << endl;
        cout << "M[" << k1 << "] -> " << M[k1] << endl;
        cout << "M[" << k2 << "] -> " << M[k2] << endl;
        cout << "size() -> " << M.size() << endl << endl;
    }

    {
        cout << "contains(" << k1 << ") -> " << M.contains(k1) << endl;
        cout << "remove(" << k1 << ") -> " << M.remove(k1) << endl;
        cout << "size() -> " << M.size() << endl;
        cout << "contains(" << k1 << ") -> " << M.contains(k1) << endl;
        cout << "remove(" << k1 << ") -> " << M.remove(k1) << endl;
        cout << "size() -> " << M.size() << endl << endl;
        cout << "put(" << k1 << ", \"value\") -> " << M.put(k1, "value") << endl;
        cout << "put(" << k1 << ", \"value\") -> " << M.put(k1, "value") << endl;
        cout << "M[" << k1 << "] -> " << M[k1] << endl;
    }

    {
        cout << "value assignment: M[<1, 2, 3>] := \"changed\"" << endl;
        M[{1, 2, 3}] = "changed";
        cout << "M[<1, 2, 3>] -> " << M[{1, 2, 3}] << endl;
        cout << "M = " << M << endl;
        cout << "size() -> " << M.size() << endl << endl;
    }

    {
        auto pos = M.begin();
        pos++;
        cout << "remove by iterator pointing to "
             << pos->first << " -> " << pos->second << ":" << endl;
        auto key = pos->first;
        M.remove(pos);
        cout << "iterator now points to " << pos->first << " -> " << pos->second << endl;
        cout << "contains(" << key << ") -> " << M.contains(key) << endl;
        cout << "size() -> " << M.size() << endl << endl;
    }

    {
        M.clear();
        cout << "clear() -> M = " << M << endl;
        cout << "size() -> " << M.size() << endl << endl;
    }
}
