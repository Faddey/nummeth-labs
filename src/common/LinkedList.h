#ifndef NUMMETH_LINKEDLIST_H
#define NUMMETH_LINKEDLIST_H

template <typename T>
struct LinkedList {
    typedef T value_type;

    struct Node {
        typedef T value_type;

        T data;
        Node *prev;
        Node *next;

        Node(const T& data=T(), Node *prev=nullptr, Node *next=nullptr)
                : data(data), prev(prev), next(next) {}
    };
    
    Node* head;
    size_t size;

    LinkedList(Node* node, size_t size) : head(node), size(size) {}
    LinkedList(): LinkedList(nullptr, 0) {}

    void insertAfter(Node* new_n, Node* n) {
        new_n->prev = n;
        new_n->next = n->next;
        n->next = new_n;
        if (new_n->next != nullptr) {
            new_n->next->prev = new_n;
        }
        size++;
    }
    void insertBefore(Node* new_n, Node* n) {
        new_n->next = n;
        new_n->prev = n->prev;
        n->prev = new_n;
        if (new_n->prev != nullptr) {
            new_n->prev->next = new_n;
        }
        if (n == head) {
            head = new_n;
        }
        size++;
    }
    Node* remove(Node* n) {
        if (n->prev != nullptr) {
            n->prev->next = n->next;
        }
        if (n->next != nullptr) {
            n->next->prev = n->prev;
        }
        if (n == head) {
            head = n->next;
        }
        size--;
        Node *next = n->next;
        delete n;
        return next;
    }
    void clear(Node *start=nullptr) {
        size = 0;
        Node *n = start ? start : head;
        Node *prev = n ? n->prev : nullptr;
        while(n != nullptr) {
            Node *next = n->next;
            delete n;
            n = next;
        }
        if (prev != nullptr) {
            prev->next = nullptr;
        } else {
            head = nullptr;
        }
    }
};


#endif //NUMMETH_LINKEDLIST_H
