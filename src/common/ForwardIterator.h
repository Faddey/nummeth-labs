#ifndef NUMMETH_FORWARDITERATOR_H
#define NUMMETH_FORWARDITERATOR_H


template<typename V>
class ForwardIterator: public std::iterator<std::forward_iterator_tag, V> {
public:

    class Impl {
    public:
        virtual Impl* copy() const = 0;
        virtual void advance() = 0;
        virtual bool operator==(const Impl& other) const = 0;
        bool operator!=(const Impl& other) const { return !(*this == other); }
        virtual V& getValue() = 0;
        virtual ~Impl() {}
    };
    
    ForwardIterator(const ForwardIterator<V>& other): impl(other.impl->copy()) {}
    ForwardIterator(Impl* impl): impl(impl) {}
    bool operator!=(const ForwardIterator& it) const {
        return *impl != *it.impl;
    }
    bool operator==(const ForwardIterator& it) const {
        return !(*this != it);
    }
    ForwardIterator<V>& operator++() {
        impl->advance();
        return *this;
    }
    ForwardIterator<V>& operator++(int) {
        return ++*this;
    }
    V& operator*() const {
        return impl->getValue();
    }
    V* operator->() const {
        return &impl->getValue();
    }
    virtual ~ForwardIterator() {
        delete impl;
    }

protected:

    Impl* getImpl() const { return impl; }

private:

    Impl* impl;
};

#endif //NUMMETH_FORWARDITERATOR_H
