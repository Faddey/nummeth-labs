#include <iostream>
#include <sstream>
#include <functional>


using namespace std;


bool readDec(unsigned& out, const char* s) {
    stringstream ss(s);
    ss >> out;
    return !ss.fail();
}
bool readBin(unsigned& out, const char* s) {
    out = 0;
    while(*s != 0) {
        if (*s != '1' && *s != '0') return false;
        out <<= 1;
        out += (*s=='1');
        s++;
    }
    return true;
}
bool readHex(unsigned& out, const char* s) {
    stringstream ss(s);
    ss >> hex >> out;
    return !ss.fail();
}

string formatDec(unsigned n) {
    stringstream ss;
    ss << n;
    return ss.str();
}
string formatBin(unsigned n) {
    char buf[sizeof(int)*8];
    int sz = 0;
    for(; n > 0; sz++, n >>= 1) {
        buf[sz] = (n%2) ? '1' : '0';
    }
    stringstream ss;
    for(int i = sz-1; i >= 0; i--) {
        ss << buf[i];
    }
    return ss.str();
}
string formatHex(unsigned n) {
    stringstream ss;
    ss << hex << n;
    return ss.str();
}


enum Format { DECIMAL = 0, BIN = 1, HEX = 2};
function<bool(unsigned&, const char*)> readers[] = { readDec, readBin, readHex };
function<string(unsigned)> formatters[] = { formatDec, formatBin, formatHex };


struct Params {
    Format fmt = DECIMAL;
    bool isGray = false;

    bool parse(const char* str) {
        while(*str != 0) {
            if (*str == 'd') fmt = DECIMAL;
            else if (*str == 'b') fmt = BIN;
            else if (*str == 'h') fmt = HEX;
            else if (*str == 'g') isGray = true;
            else if (*str == 'n') isGray = false;
            else return false;
            str++;
        }
        return true;
    }
};


unsigned encodeGray(unsigned n) {
    return n ^ (n >> 1);
}
unsigned decodeGray(unsigned n) {
    unsigned int r;
    for (r = 0; n; n >>= 1) {
        r ^= n;
    }
    return r;
}


int main(int argc, char** argv) {
    if (argc < 4) return 1;
    Params in, out;
    if (!in.parse(argv[1])) return 2;
    if (!out.parse(argv[2])) return 3;


    for(int arg = 3; arg < argc; arg++) {
        unsigned n = 0;
        if (!readers[in.fmt](n, argv[arg])) return arg + 1;
        if (in.isGray != out.isGray)
            n = out.isGray ? encodeGray(n) : decodeGray(n);
        auto s = formatters[out.fmt](n);
        cout << s << endl;
    }

    return 0;
}
